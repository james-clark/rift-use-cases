# rift-use-cases

Sample use-cases / demos for the RIFT parameter estimation analysis

## Contents

 * [rsv](https://git.ligo.org/james-clark/rift-use-cases/-/tree/master/rsv): standalone ILE jobs for [RSV monitor](http://cabinet-10-10-4.t2.ucsd.edu/rsv/)
