# Standalone RIFT jobs for RSVmon

Included herein are completely standalone RIFT jobs for (e.g.) [RSV
monitoring](http://cabinet-10-10-4.t2.ucsd.edu/rsv/).
Each job should run for ~30 minutes and uses 1 GPU + 1 CPU.

These jobs run single `integrate_extrinsic_likelihood` (ILE) only; no pre- or
post-processing is included and the jobs are not part of a DAG.

Two types of job are provided:

1. [`rift-private-cvmfs`](https://git.ligo.org/james-clark/rift-use-cases/-/tree/master/rsv/rift-private-cvmfs):
   Reads GW150914 frame data from authenticated LIGO CVMFS. Authentication via
   X509 proxy.
2. [`rift-no-cvmfs`](https://git.ligo.org/james-clark/rift-use-cases/-/tree/master/rsv/rift-no-cvmfs):
   Reads GW150914 from publicly-available frame files which are included with the
   job and copied to the execute host via condor file transfer (total frame file
   data volume is 240 MB)

The jobs are run via a wrapper script which also checks Nvidia / Cupy
functionality prior to executing the RIFT application.

Also included are bash scripts (`rift-private-cvmfs.sh`, `rift-no-cvmfs.sh`)
which execute the same command (in singularity) via the command line. If you use
these **check that the bindings are appropriate for your host**.

## How to submit to condor

Paths are set relative to the directory containing the submit files - jobs
should be submitted from inside each directory:

```
pushd rift-private-cvmfs
condor_submit rift-private-cvmfs.sub
popd
```
