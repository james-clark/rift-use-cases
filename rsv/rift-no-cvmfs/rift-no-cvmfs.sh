#!/bin/bash -e

# From submit file
executable=rift-ile-wrapper.sh
arguments=" --save-P 0.1 --fmax 511.0 --cache frames.cache --event-time 1126259462.4264345 --channel-name H1=LOSC-STRAIN --psd-file H1=H1-psd.xml.gz --fmin-ifo H1=20 --channel-name L1=LOSC-STRAIN --psd-file L1=L1-psd.xml.gz --fmin-ifo L1=20 --fmin-template 20.0 --reference-freq 20 --d-max 3000 --data-start-time 1126259460.4264345 --data-end-time 1126259464.4264345 --inv-spec-trunc-time 0 --window-shape 0.2 --time-marginalization --inclination-cosine-sampler --declination-cosine-sampler --n-max 4000000 --n-eff 50 --vectorized --gpu --no-adapt-after-first --no-adapt-distance --srate 4096 --adapt-weight-exponent 0.3 --l-max 2 --approx IMRPhenomD --vectorized --gpu  --n-events-to-analyze 20 --sim-xml .//overlap-grid-0.xml.gz  --output-file CME_out-0-0-0.xml --cache-file frames.cache --event 0"
SingularityImage="/cvmfs/singularity.opensciencegrid.org/james-clark/research-projects-rit/rift:latest"

echo "Running ILE in singularity:"
singularity exec --nv \
    --bind /home \
    --no-home --containall --pwd $PWD \
    $SingularityImage /bin/bash $executable ${arguments}
