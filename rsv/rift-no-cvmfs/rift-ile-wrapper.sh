#!/bin/bash -e

# Check GPU works:
echo "Checking nvidia-smi:"
/usr/bin/nvidia-smi

echo "Checking cupy"
python -c "import cupy; cupy.show_config(); cupy.array(5)"
echo "Cupy ok!"

# Call ILE
/usr/local/bin/integrate_likelihood_extrinsic_batchmode $@
